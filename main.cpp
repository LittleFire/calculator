#include <stdio.h>
#include "resource.h"
using namespace std;

void intro();
float add(float, float);
float sub(float, float);
float mul(float, float);
float div(float, float);

int main(void)
{
    int opt;
    intro();
    while(true)
    {
        do
        {
            opt = -1;
            printf("Enter option: ");
            scanf("%d", &opt);
        } while(!(opt>=-1 && opt<=4));
        if(opt != -1)
        {
            if(opt == 0) intro();
            else
            {
                float num1 = 0;
                float num2 = 0;

                printf("\tNumber #1: ");
                scanf("%f", &num1);
                printf("\tNumber #2: ");
                scanf("%f", &num2);

                printf("\t\t");
                if(        opt==1) printf("Result: %f + %f = %f\n", num1, num2, add(num1, num2));
                else if(opt==2) printf("Result: %f - %f = %f\n", num1, num2, sub(num1, num2));
                else if(opt==3) printf("Result: %f * %f = %f\n", num1, num2, mul(num1, num2));
                else if(opt==4) printf("Result: %f / %f = %f\n", num1, num2, div(num1, num2));
                else break;
            }
        } else break;
    }
    return 0;
}

float add(float a, float b)
{
    return a+b;
}

float sub(float a, float b)
{
    return a-b;
}

float mul(float a, float b)
{
    return a*b;
}

float div(float a, float b)
{
    return a/b;
}

void intro()
{
    printf("Please enter one of these options [-1 to quit].\n");
    printf("\t1. Addition\n");
    printf("\t2. Subtraction\n");
    printf("\t3. Multiplication\n");
    printf("\t4. Division\n");
}
